第二阶段生成 service和controller

# mybatis-generator的代码自动生成工具
该插件可以使得mybatis-generator生成的代码中修改的部分可以原封不动的保留下来，如添加的新的变量，方法，注解等都会被保留。
*由于是自定义的，就限制了mybatis-generator扩展性，所有代码标准都已经统一。*


## 一. 新方式：无需任何配置文件，使用插件直接生成代码
**此方式会读取数据库中的表元数据，来自动生成全部表的mybatis代码**

			<plugin>
				<groupId>corg.jeff.plugin</groupId>
				<artifactId>mybatis-generator-maven-plugin</artifactId>
				<version>1.0.0-SNAPSHOT</version>
				<configuration>
					<project implementation="org.apache.maven.project.MavenProject" />
					<jdbcDriverClass>com.mysql.jdbc.Driver</jdbcDriverClass>
					<jdbcURL> jdbc:mysql://192.168.1.48:3306/turtle_warehouse</jdbcURL>
					<jdbcUser>root</jdbcUser>
					<jdbcPassword>Sonar@1234</jdbcPassword>
					<targetJavaProject>target</targetJavaProject>
					<targetModelPackage>com.htdc.turtle.entity</targetModelPackage>
					<targetMapperPackage>com.htdc.turtle.mapper</targetMapperPackage>
					<targetResourcesProject>target</targetResourcesProject>
					<targetXMLPackage>com.htdc.turtle.mapper.xml</targetXMLPackage>
					<trimStrings>false</trimStrings>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>mysql</groupId>
						<artifactId>mysql-connector-java</artifactId>
						<version>5.1.39</version>
					</dependency>
				</dependencies>
			</plugin>
			
**有时候，表的字段类型需要自定义,可以引入xml的方式来自动生成代码**
	
	
	/**
	 * 采用新模式并考虑使用xml引入table时的模式。引入xml必须指定
	 * ONLY_TABLE	只从表中生成代码(默认)
	 * ONLY_XML		只从XML中生成代码
	 * TABLE_XML		两者都可以, 同样的table名，xml生成优先
	 */
	 xmlTableMode	
	 //包含table描述的xml，支持多地址，以逗号分割。当模式为ONLY_XML或者TABLE_XML时, 必须配置该变量
	 tableXMLConfigs
	 
配置如下：

		<plugin>
				<groupId>corg.jeff.plugin</groupId>
                <artifactId>mybatis-generator-maven-plugin</artifactId>
                <version>1.0.0-SNAPSHOT</version>
				<configuration>
					<project implementation="org.apache.maven.project.MavenProject" />
					<xmlTableMode>TABLE_XML</xmlTableMode>
					<tableXMLConfigs>${basedir}/src/main/resources/generate/a.xml</tableXMLConfigs>
					<jdbcDriverClass>com.mysql.jdbc.Driver</jdbcDriverClass>
					<jdbcURL> jdbc:mysql://192.168.1.48:3306/turtle_warehouse</jdbcURL>
					<jdbcUser>root</jdbcUser>
					<jdbcPassword>Sonar@1234</jdbcPassword>
					<targetJavaProject>target</targetJavaProject>
					<targetModelPackage>com.htdc.turtle.entity</targetModelPackage>
					<targetMapperPackage>com.htdc.turtle.mapper</targetMapperPackage>
					<targetResourcesProject>target</targetResourcesProject>
					<targetXMLPackage>com.htdc.turtle.mapper.xml</targetXMLPackage>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>mysql</groupId>
						<artifactId>mysql-connector-java</artifactId>
						<version>5.1.39</version>
					</dependency>
				</dependencies>
			</plugin>
			
a.xml代码如下：
	
	<tables>
		<table tableName="AA" schema="Aa" delimitAllColumns="true" tableType="TABLE">
  		<ignoreColumn column="name"/>
  		<columnOverride column="sex" javaType="java.lang.Boolean"/>
  		<columnOverride column="DESC"  delimitedColumnName="true" />
		</table>
	</tables>
	
**有时候表名附带特殊的前缀,可以配置去掉; 支持多字符串过滤,以逗号分割;仅在table中有效,xml配置方式会覆盖**

	<targetTablePrefixs>t_,X_</targetTablePrefixs>
	


## 二. 老方式：按mybatis原生的配置方式生成代码. 不支持属性文件. 不支持自定义插件.
					<plugin>
						<groupId>corg.jeff.plugin</groupId>
                        <artifactId>mybatis-generator-maven-plugin</artifactId>
                        <version>1.0.0-SNAPSHOT</version>
						<configuration>
							<project implementation="org.apache.maven.project.MavenProject" />
							<isOld>true</isOld>
							<generateConfig>${basedir}/src/main/resources/generate/generatorConfig.xml</generateConfig>
						</configuration>
						<dependencies>
							<dependency>
								<groupId>mysql</groupId>
								<artifactId>mysql-connector-java</artifactId>
								<version>5.1.39</version>
							</dependency>
						</dependencies>
					</plugin>

请在generatorConfig.xml中设置plugin如下：

	<plugin type="MyCustomPlugin" />
	
## 三. 内置sql创建内存数据库的方式,兼容上面两种形式
使用配置		isGegerateCodeBySql	和sqlConfig来读取sql创建内存数据库	。注意：由于使用了H2来创建表，有些非标准语法是不支持的。			

					<plugin>
						<groupId>org.jeff.plugin</groupId>
                        <artifactId>mybatis-generator-maven-plugin</artifactId>
                        <version>1.0.0-SNAPSHOT</version>
						<configuration>
							<project implementation="org.apache.maven.project.MavenProject" />
							<debugMode>true</debugMode>
							<isOld>false</isOld>
							<xmlTableMode>ONLY_TABLE</xmlTableMode>
							<targetJavaProject>target</targetJavaProject>
							<targetModelPackage>com.htdc.turtle.entity</targetModelPackage>
							<targetMapperPackage>com.htdc.turtle.mapper</targetMapperPackage>
							<targetResourcesProject>target</targetResourcesProject>
							<targetXMLPackage>com.htdc.turtle.mapper.xml</targetXMLPackage>
							<mapperPlugin>MyCustomPlugin</mapperPlugin>
							<targetTablePrefixs>t_</targetTablePrefixs>
							
							<isGegerateCodeBySql>true</isGegerateCodeBySql>
							<sqlConfig>${basedir}/src/main/resources/sql/a.sql</sqlConfig>
						</configuration>
						<dependencies>
							<dependency>
								<groupId>mysql</groupId>
								<artifactId>mysql-connector-java</artifactId>
								<version>5.1.39</version>
							</dependency>
						</dependencies>
					</plugin>
					
					
## 四. 其他参数
	<trimStrings>false</trimStrings>	生成实体代码时，是否屏蔽字符串的trim方法,默认不生成trim

## 开发提醒
### 使用自定义的插件，需在maven的settings.xml配置
	<pluginGroups>
		<pluginGroup>org.jeff.plugin</pluginGroup>
	</pluginGroups>
    
### maven执行指令：
	mvn codegenerate:generate
	
### 部署发布
	mvn clean deploy -Phtdc
	
## git小技巧
    git remote add origin-chiway http://192.168.37.168:3000/jeff/mybatis-generator-maven-plugin.git
    git remote -v
    git push origin
    git push origin-chiway