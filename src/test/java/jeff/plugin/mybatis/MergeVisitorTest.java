package jeff.plugin.mybatis;

import jeff.plugin.mybatis.code.visitor.MergeUtil;

import jeff.plugin.mybatis.util.IOUtil;
import jeff.plugin.mybatis.util.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class MergeVisitorTest {

//    @Test
//    public void test1(){
//        String oldSource = ResourceUtil.getStreamAsString("java.old");
//        String newSource = ResourceUtil.getStreamAsString("java.new");
//
//        String expect = ResourceUtil.getStreamAsString("java.expect");
//        String merge = MergeUtil.merge(oldSource, newSource);
////        System.out.println(merge);
//        Assert.assertEquals("", expect, merge);
//    }

    @Test
    public void testInterface() throws IOException {
        String oldSource = IOUtils.readFromResource("interface.old");

        String newSource = IOUtils.readFromResource("interface.new");
        String merge = MergeUtil.merge(oldSource, newSource);
        System.out.println(merge);

        String expect = IOUtils.readFromResource("interface.expect");

        Assert.assertEquals("", expect, merge);
    }

}
