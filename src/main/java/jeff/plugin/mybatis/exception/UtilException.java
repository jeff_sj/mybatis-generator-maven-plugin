package jeff.plugin.mybatis.exception;

import jeff.plugin.mybatis.util.StringUtil;

public class UtilException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UtilException() {
		super();
	}

	public UtilException(String message, Throwable cause,
                         boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UtilException(String message, Throwable cause) {
		super(message, cause);
	}

	public UtilException(String message) {
		super(message);
	}
	
	public UtilException(String message, Object ...args) {
		super(StringUtil.substitute(message, args));
	}

	public UtilException(Throwable cause) {
		super(cause);
	}

	public UtilException(Throwable throwable, String messageTemplate, Object... params) {
		super(StringUtil.substitute(messageTemplate, params), throwable);
	}

}
