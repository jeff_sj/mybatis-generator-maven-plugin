package jeff.plugin.mybatis.exception;

/**
 * 通过异常类; 常用于SAssert的判断验证
 * @author Jeff
 */
public class ValidateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ValidateException(String message) {
		super(message);
	}

	public ValidateException(Throwable throwable) {
		super(throwable);
	}

	public ValidateException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
}
