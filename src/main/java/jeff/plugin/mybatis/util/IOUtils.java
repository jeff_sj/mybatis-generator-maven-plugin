package jeff.plugin.mybatis.util;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

public class IOUtils {

	/**
	 * 代表流结束的标记
	 */
	public static final int EOF = -1;
	
	 private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
	
	 //start----------------------关闭流------------------------------------
	 
	public static void closeQuietly(final InputStream input) {
        closeQuietly((Closeable) input);
    }

    public static void closeQuietly(final OutputStream output) {
        closeQuietly((Closeable) output);
    }
	
    public static void closeQuietly(final Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (final IOException ioe) {
            // ignore
        }
    }
    
    public static void closeQuietly(final Closeable... closeables) {
        if (closeables == null) {
            return;
        }
        for (final Closeable closeable : closeables) {
            closeQuietly(closeable);
        }
    }
    
  //end----------------------关闭流------------------------------------
    
  //start----------------------流内容复制------------------------------------
    
    public static int copy(final InputStream input, final OutputStream output) throws IOException {
        final long count = copyLarge(input, output);
        if (count > Integer.MAX_VALUE) {
            return -1;
        }
        return (int) count;
    }
    
    public static long copy(final InputStream input, final OutputStream output, final int bufferSize)
            throws IOException {
        return copyLarge(input, output, new byte[bufferSize]);
    }
    
    public static long copyLarge(final InputStream input, final OutputStream output) throws IOException {
        return copy(input, output, DEFAULT_BUFFER_SIZE);
    }
    
    public static long copyLarge(final InputStream input, final OutputStream output, final byte[] buffer) throws IOException {
        long count = 0;
        int n;
        while (EOF != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
    
  //end----------------------流内容复制------------------------------------
	
  //start----------------------内容读取------------------------------------
    
    public static String read(InputStream in) {
        InputStreamReader reader;
        try {
            reader = new InputStreamReader(in, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
        return read(reader);
    }
    
    public static String read(Reader reader) {
        try {
            StringWriter writer = new StringWriter();

            char[] buffer = new char[DEFAULT_BUFFER_SIZE];
            int n = 0;
            while (-1 != (n = reader.read(buffer))) {
                writer.write(buffer, 0, n);
            }

            return writer.toString();
        } catch (IOException ex) {
            throw new IllegalStateException("read error", ex);
        }
    }
    
    public static byte[] readByteArray(final InputStream input) throws IOException {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        copy(input, output);
        return output.toByteArray();
    }
    
    /**
     * 从当前工程或者jar中读取文件;
     * @param resource
     * @return
     * @throws IOException
     */
    public static byte[] readByteArrayFromResource(String resource) throws IOException {
        InputStream in = null;
        try {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
            if (in == null) {
                in = IOUtils.class.getResourceAsStream(resource);
            }
            if (in == null) {
                return null;
            }

            return readByteArray(in);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }
    
    /**
     * 从当前工程或者jar中读取文件;
     * @param resource
     * @return
     * @throws IOException
     */
    public static String readFromResource(String resource) throws IOException {
        InputStream in = null;
        try {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
            if (in == null) {
                in = IOUtils.class.getResourceAsStream(resource);
            }

            if (in == null) {
                return null;
            }

            String text = IOUtils.read(in);
            return text;
        } finally {
        	IOUtils.closeQuietly(in);
        }
    }
    
    /**
     * 从资源文件中读取文件流
     * @param resource
     * @return
     * @throws IOException
     */
    public static InputStream readInputStreamFromResource(String resource) throws IOException {
         InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
        if (in == null) {
            in = IOUtils.class.getResourceAsStream(resource);
        }

        if (in == null) {
            return null;
        }
        return in;
    }
    
  //end----------------------内容读取------------------------------------
    
}
