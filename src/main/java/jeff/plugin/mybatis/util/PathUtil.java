package jeff.plugin.mybatis.util;

/**
 * 路径获取工具类
 * @author Jeff
 */
public class PathUtil {

	/**
	 * 获取整个class加载器的路径；如/E:/repository/oschina/javatools/target/classes/
	 */
	public static String getClassPath(Class<?> cla){
		return cla.getProtectionDomain().getCodeSource().getLocation().getPath();
	}
	
	/**
	 * 获取非web项目的target目录；如如/E:/repository/oschina/javatools/target/
	 * @param cla
	 * @return
	 */
	public static String getTargetPath(Class<?> cla){
		String path = getClassPath(cla);
		if(path == null || path.indexOf("") == -1){
			return null;
		}
		int index = path.indexOf("target");
		return path.substring(0, index) + "target";
	}

	/**
	 * 获取web项目根路径
	 * @param cla class
	 */
	public static String getWebRoot(Class<?> cla){
		String path = cla.getProtectionDomain().getCodeSource().getLocation().getPath();
		int index = path.indexOf("WEB-INF");
		if(index == -1){ 
			return null;
		}
		return path.substring(0, index);
	}
	
}
