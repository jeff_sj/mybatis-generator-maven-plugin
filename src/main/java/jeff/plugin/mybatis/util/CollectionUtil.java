package jeff.plugin.mybatis.util;

import java.util.*;

/**
 * 集合工具类
 */
public class CollectionUtil {

    public static <T> boolean isEmpty(T[] array){
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    public static boolean isEmpty(Iterator<?> iterator) {
        return null == iterator || !iterator.hasNext();
    }

    public static boolean isNotEmpty(Iterator<?> iterator) {
        return !isEmpty(iterator);
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    public static boolean isEmpty(Enumeration<?> enumeration) {
        return null == enumeration || !enumeration.hasMoreElements();
    }

    public static boolean isNotEmpty(Enumeration<?> enumeration) {
        return !isEmpty(enumeration);
    }


    /**
     * 新建list
     * @param enumration {@link Enumeration}
     * @param <T> 泛型
     * @return ArrayList
     */
    public static <T> List<T> list(Enumeration<T> enumration){
        final List<T> list = new ArrayList<>();
        if (null != enumration) {
            while (enumration.hasMoreElements()) {
                list.add(enumration.nextElement());
            }
        }
        return list;
    }

    /**
     * 新建list
     * @param iter {@link Iterator}
     * @param <T> 泛型
     * @return ArrayList
     */
    public static <T> List<T> list(Iterator<T> iter) {
        final List<T> list = new ArrayList<>();
        if (null != iter) {
            while (iter.hasNext()) {
                list.add(iter.next());
            }
        }
        return list;
    }


    /**
     * 如果集合为null则返回空的{@link ArrayList}，否则返回自身
     * @param list 检查对象
     * @param <T> 泛型
     * @return 集合
     */
    public static <T> List<T> wrapEmptyIfNull(List<T> list){
        return list == null ? new ArrayList<>() : list;
    }

    /**
     * 如果map为null，则返回空的{@link HashMap}，否则返回自身
     * @param map 检查对象
     * @param <K> 泛型
     * @param <V> 泛型
     * @return map
     */
    public static <K,V> Map<K,V> wrapEmptyIfNull(Map<K,V> map){
        return map == null ? new HashMap<>() : map;
    }

    // --------------------------find-------------------------------------- start

    /**
     * 找到任意一个对象
     * @param list 集合
     * @return 可选对象
     */
    public static <T> Optional<T> findAny(Iterator<T> list){
        if(list == null || !list.hasNext()){
            return Optional.empty();
        }
        return Optional.ofNullable(list.next());
    }

    /**
     * 找到任意一个对象
     * @param list 集合
     * @return 可选对象
     */
    public static <T> Optional<T> findAny(Iterable<T> list){
        return findAny(list != null ? list.iterator() : null);
    }

    // --------------------------find-------------------------------------- end

}
