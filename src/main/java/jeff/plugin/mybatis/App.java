package jeff.plugin.mybatis;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import jeff.plugin.mybatis.util.DOMUtils;
import jeff.plugin.mybatis.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import jeff.plugin.mybatis.util.IOUtils;

public class App {

	public static void main(String[] args) throws Exception {
		InputSource inputSource = new InputSource(
				IOUtils.readInputStreamFromResource("internal/generatorConfig.xml"));

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setValidating(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(inputSource);
		Element rootNode = document.getDocumentElement();
		Element contextElement = DOMUtils.getChildElementByTagName(rootNode, "context");
		

		System.out.println("====遍历====");
		for (Node n = contextElement.getFirstChild(); n != null; n = n.getNextSibling()) {
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				System.out.println(n.getNodeName());
			}
		}
		
		Element newElement = document.createElement("table");
		newElement.setAttribute("tableName", "HELLO1");
		newElement.setAttribute("domainObjectName", "Hello1");
		contextElement.appendChild(newElement);

		System.out.println("====遍历1====");
		DocumentType dtype = document.getDoctype();
		System.out.println("========>"+dtype.getInternalSubset());
		System.out.println(XMLUtils.toString(document));
	}

}
