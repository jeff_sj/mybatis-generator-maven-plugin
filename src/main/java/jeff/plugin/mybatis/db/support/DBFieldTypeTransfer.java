package jeff.plugin.mybatis.db.support;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * 数据库类型和实体变量类型转换
 * @author hp
 */
public class DBFieldTypeTransfer {

	public static Object parseType(ResultSet result, Integer i, int type, Field field) throws SQLException{
		switch(type){
			case Types.CHAR:
	        case Types.VARCHAR:
	        case Types.LONGVARCHAR: {
	            return result.getString(i);
	        }
	        //
	        case Types.BOOLEAN: {
                return result.getBoolean(i);
            }
	        //
	        case Types.DATE:{
	        	return result.getDate(i);
	        }
	        case Types.TIME:{
	        	return result.getTime(i);
	        }
	        case Types.TIMESTAMP:{
	        	return result.getTimestamp(i);
	        }
	        //
	        case Types.NUMERIC:{
	        	BigDecimal bd = result.getBigDecimal(i);
	        	if (bd == null || result.wasNull()) {
                    return null;
                }
	        	if("int".equals(field.getType().getName()) || field.getType() == Integer.class){
	        		return bd.intValueExact();
	        	}
	        	else if("long".equals(field.getType().getName()) || field.getType() == Long.class){
	        		return bd.longValueExact();
	        	}
	        	else if("double".equals(field.getType().getName()) || field.getType() == Double.class){
	        		return bd.doubleValue();
	        	}
	        	else{
	        		return bd.toPlainString();
	        	}
	        }
	        //
	        case Types.FLOAT: {
                String s = result.getString(i);
                if (result.wasNull() || s == null) {
                    return null;
                }
                return Double.parseDouble(s);
            }
	        //
	        case Types.NULL: {
                return null;
            }
	        default:{
	        	return result.getObject(i);
	        }
		}
	}
	
	public static int toJDBCType(String type) {
        if (type == null) {
            return Types.NULL;
        } else if (type.equalsIgnoreCase("NULL")) {
            return Types.NULL;
        } else if (type.equalsIgnoreCase("TINYINT")) {
            return Types.TINYINT;
        } else if (type.equalsIgnoreCase("SMALLINT")) {
            return Types.SMALLINT;
        } else if (type.equalsIgnoreCase("INTEGER")) {
            return Types.INTEGER;
        } else if (type.equalsIgnoreCase("BIGINT")) {
            return Types.BIGINT;
        } else if (type.equalsIgnoreCase("REAL")) {
            return Types.REAL;
        } else if (type.equalsIgnoreCase("FLOAT")) {
            return Types.FLOAT;
        } else if (type.equalsIgnoreCase("DOUBLE")) {
            return Types.DOUBLE;
        } else if (type.equalsIgnoreCase("DECIMAL")) {
            return Types.DECIMAL;
        } else if (type.equalsIgnoreCase("NUMERIC")) {
            return Types.NUMERIC;
        } else if (type.equalsIgnoreCase("BIT")) {
            return Types.BIT;
        } else if (type.equalsIgnoreCase("BOOLEAN")) {
            return Types.BOOLEAN;
        } else if (type.equalsIgnoreCase("BINARY")) {
            return Types.BINARY;
        } else if (type.equalsIgnoreCase("VARBINARY")) {
            return Types.VARBINARY;
        } else if (type.equalsIgnoreCase("LONGVARBINARY")) {
            return Types.LONGVARBINARY;
        } else if (type.equalsIgnoreCase("CHAR")) {
            return Types.CHAR;
        } else if (type.equalsIgnoreCase("VARCHAR")) {
            return Types.VARCHAR;
        } else if (type.equalsIgnoreCase("LONGVARCHAR")) {
            return Types.LONGVARCHAR;
        } else if (type.equalsIgnoreCase("DATE")) {
            return Types.DATE;
        } else if (type.equalsIgnoreCase("TIME")) {
            return Types.TIME;
        } else if (type.equalsIgnoreCase("TIMESTAMP")) {
            return Types.TIMESTAMP;
        } else if (type.equalsIgnoreCase("CLOB")) {
            return Types.CLOB;
        } else if (type.equalsIgnoreCase("BLOB")) {
            return Types.BLOB;
        } else if (type.equalsIgnoreCase("ARRAY")) {
            return Types.ARRAY;
        } else if (type.equalsIgnoreCase("STRUCT")) {
            return Types.STRUCT;
        } else if (type.equalsIgnoreCase("REF")) {
            return Types.REF;
        } else if (type.equalsIgnoreCase("DATALINK")) {
            return Types.DATALINK;
        } else if (type.equalsIgnoreCase("DISTINCT")) {
            return Types.DISTINCT;
        } else if (type.equalsIgnoreCase("JAVA_OBJECT")) {
            return Types.JAVA_OBJECT;
        } else if (type.equalsIgnoreCase("SQLXML")) {
            return Types.SQLXML;
        } else if (type.equalsIgnoreCase("ROWID")) {
            return Types.ROWID;
        }
        return Types.OTHER;
    }
	
}
