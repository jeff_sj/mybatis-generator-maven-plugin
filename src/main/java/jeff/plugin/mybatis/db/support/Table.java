package jeff.plugin.mybatis.db.support;

import java.util.List;

/**
 * table
 * @author Jeff
 */
public class Table {

	private String name;
	private String type;
	private String remarks;
	
	private List<TableColumn> columns;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<TableColumn> getColumns() {
		return columns;
	}
	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name + " " + remarks + " " + type + " (\n");
		if(columns != null){
			for(TableColumn column : columns){
				sb.append("\t").append(column.getColumnName());
				sb.append("\t").append(column.getTypeName());
				if(column.isPrimarykey()){
					sb.append("\t").append("primay key");	
				}
				sb.append("\t").append(column.getRemarks());
				sb.append(",\n");
			}
		}
		sb.append(")");
		return sb.toString();
	}
}
