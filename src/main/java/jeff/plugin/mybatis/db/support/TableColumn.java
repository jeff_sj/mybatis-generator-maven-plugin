package jeff.plugin.mybatis.db.support;

/**
 * tabel column
 * @author Jeff
 */
public class TableColumn {

	private String columnName;
	private int dataType;
	private String typeName;
	private String remarks;
	private boolean primarykey;
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public int getDataType() {
		return dataType;
	}
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public boolean isPrimarykey() {
		return primarykey;
	}
	public void setPrimarykey(boolean primarykey) {
		this.primarykey = primarykey;
	}
}
