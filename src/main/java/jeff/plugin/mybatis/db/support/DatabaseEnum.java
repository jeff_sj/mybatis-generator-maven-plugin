package jeff.plugin.mybatis.db.support;

public enum DatabaseEnum {

	ORACLE("jdbc:oracle:thin", "oracle.jdbc.driver.OracleDriver"),	//jdbc:oracle:thin:@localhost:1521:database
	MYSQL("jdbc:mysql", "com.mysql.jdbc.Driver"),	//jdbc:mysql://localhost/database
	SQLSERVER("jdbc:microsoft:sqlserver", "com.microsoft.jdbc.sqlserver.SQLServerDriver"),	//jdbc:microsoft:sqlserver://localhost:1433;DatabaseName=database
	DB2("jdbc:db2", "com.ibm.db2.jdbc.app.DB2Driver"),	//jdbc:db2://localhost:5000/database
	H2("jdbc:h2", "org.h2.Driver");	//jdbc:h2:mem:test
	
	private String driver;
	private String simpleName;
	DatabaseEnum(String simpleName, String driver){
		this.simpleName = simpleName;
		this.driver = driver;
	}
	
	public static DatabaseEnum getDatabaseByUrl(String url){
		if(url == null || url.trim().equals("")){
			return null;
		}
		for(DatabaseEnum e : DatabaseEnum.values()){
			if(url.toLowerCase().contains(e.getSimpleName())){
				return e;
			}
		}
		return null;
	}
	
	
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getSimpleName() {
		return simpleName;
	}
	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}
}
