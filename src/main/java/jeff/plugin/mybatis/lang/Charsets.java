package jeff.plugin.mybatis.lang;

import java.nio.charset.Charset;

/**
 * 字符编码集合
 * @author Jeff
 */
public final class Charsets {

	private Charsets() {
	    throw new AssertionError("No jexx.lang.Charsets instances for you!");
	}

	public static final Charset US_ASCII = Charset.forName("US-ASCII");
	
	public static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
	
	public static final Charset UTF_8 = Charset.forName("UTF-8");
	
	public static final Charset UTF_16BE = Charset.forName("UTF-16BE");
	
	public static final Charset UTF_16LE = Charset.forName("UTF-16LE");
	
	public static final Charset UTF_16 = Charset.forName("UTF-16");

    public static Charset toCharset(final String charset) {
        return charset == null ? Charset.defaultCharset() : Charset.forName(charset);
    }

    public static String name(Charset charset){
        return charset != null ? charset.name() : defaultCharsetName();
    }

    public static Charset defaultCharset(){
        return Charset.defaultCharset();
    }

    public static String defaultCharsetName(){
        return defaultCharset().name();
    }
	
}
