package jeff.plugin.mybatis.code.plugin;

import java.util.List;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;

/**
 * 定制化的插件, 对生成的mybatis代码做一些定制化处理
 * @author Jeff
 */
public class MyCustomPlugin extends PluginAdapter {

	@Override
	public boolean validate(List<String> warnings) {
		return true;
	}

	// sqlMap中文档头控制
	@Override
	public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
		List<Element> elements = document.getRootElement().getElements();
		int length = elements.size();
		// 每个元素加入一行空格,仅美观
		for (int i = 0; i < 2 * length; i += 2) {
			document.getRootElement().addElement(i, new TextElement(""));
		}
		return true;
	}
	
	@Override
	public boolean clientGenerated(Interface interfaze,
            TopLevelClass topLevelClass,
            IntrospectedTable introspectedTable) {
			interfaze.addAnnotation("@Repository");
			interfaze.addImportedType(new FullyQualifiedJavaType("org.springframework.stereotype.Repository"));
        return true;
    }

}
