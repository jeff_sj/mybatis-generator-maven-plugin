package jeff.plugin.mybatis.code.visitor;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import jeff.plugin.mybatis.util.StringUtil;

public class MergeUtil {

    /**
     * 合并新旧代码
     * @param oldsource 旧代码
     * @param newsource 新代码
     * @return 合并后的代码
     */
    public static String merge(String oldsource, String newsource){
        CompilationUnit oldUnit = JavaParser.parse(oldsource);
        CompilationUnit newUnit = JavaParser.parse(newsource);
        oldUnit.accept(new MergeVisitor(newUnit), null);
        return StringUtil.str(newUnit);
    }

}
