package jeff.plugin.mybatis.code.example;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.internal.rules.BaseRules;

public class MyRule extends BaseRules {

	public MyRule(IntrospectedTable introspectedTable) {
		super(introspectedTable);
	}

	@Override
	public boolean generatePrimaryKeyClass() {
		return false;
	}

	@Override
	public boolean generateBaseRecordClass() {
		return introspectedTable.hasBaseColumns();
	}

	@Override
	public boolean generateRecordWithBLOBsClass() {
		return introspectedTable.hasBLOBColumns();
	}
	
	@Override
	public boolean generateBaseColumnList() {
		return true;
	}
	
	

}
